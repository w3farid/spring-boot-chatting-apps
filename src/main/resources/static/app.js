var stompClient = null;

function setConnected(connected) {
    // @ts-ignore
    $("#connect").prop("disabled", connected);
    // @ts-ignore
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        // @ts-ignore
        $("#conversation").show();
    }
    else {
        // @ts-ignore
        $("#conversation").hide();
    }
    // @ts-ignore
    $("#greetings").html("");
}

function connect() {
    // @ts-ignore
    var socket = new SockJS('/gs-guide-websocket');
    // @ts-ignore
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
       // setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (greeting) {
            console.log(greeting);
            showGreeting(JSON.parse(greeting.body).content);
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    // @ts-ignore
    stompClient.send("/app/hello", {}, JSON.stringify({'name': $(".write_msg").val()}));
}

function showGreeting(message) {
    // @ts-ignore
   // $("#greetings").append("<tr><td>" + message + "</td></tr>");     
     var htmlMessage = '<div class="outgoing_msg">'
            + '<div class="sent_msg">'
            +  '<p>'+message+'</p>'
            +  '<span class="time_date"> '+getCurrentTime()+' | '+getCurrentDate()+'</span>'
            +'</div>'
          +'</div>';
        $('.write_msg').val("");
        $(".msg_history").append(htmlMessage);
}
connect();
// @ts-ignore
$(function () {
    // @ts-ignore
    
    $(".chat-action").on('submit', function (e) {
        e.preventDefault();
    });
    // @ts-ignore
    $( "#connect" ).click(function() { connect(); });
    // @ts-ignore
    $( "#disconnect" ).click(function() { disconnect(); });
    // @ts-ignore
    $( ".msg_send_btn" ).click(function() { sendName(); });
});