package com.farid.chat.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import com.farid.chat.model.Greeting;
import com.farid.chat.model.Message;

@Controller
public class MessageController {
	@MessageMapping("/hello")
	@SendTo("/topic/greetings")
	public Greeting greeting (Message message) throws Exception{
		Thread.sleep(1000);
		String name = message.getName();
		return new Greeting("Hello1, "+ HtmlUtils.htmlEscape(message.getName() +"!"));
	}
}
